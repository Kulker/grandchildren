/***
	Nowy program ma za zadanie odbierać informacje od "wnucząt" i żyć tak długo,
	aż ostatni nie zginie.
	a) rejestruje informację, jakie procesy korzystają z poszczególnych łączy
	   (PIDy)
	b) korzystając z poll czeka na komunikaty od wnucząt i wyświetla je wraz z
	   własnym komentarzem, 
	c) jeżeli żaden komunikat nie przyszedł przed upływem timeout, to wyświetla
	   informację, że nadal działa i wraca do czuwania,
	d) jeżeli zauważy, że jakieś łącze przestało działać, to sprawdza, czy
	   istnieje jeszcze proces do niego przypisany (wysyła sygnał) 
	   jeżeli istnieje, to spróbuje tego testu po następnym wybudzeniu.
***/
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

using namespace std;

int main(int argc, char const *argv[])
{
	int pipe_fd;
	pipe_fd = atoi(argv[1]);
	char bufor[100];
	pid_t pidek;

	read(pipe_fd,&bufor,sizeof(bufor));
	cout<<bufor<<endl;
	
}