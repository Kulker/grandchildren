/***
	Czynności wnuka:
		0) przedstawia się głównemu procesowi wysyłając mu swój PID,
		a) losuje ile czasu będzie spać (T1),
		b) wysyła tę informację i zasypia (na T1),
		c) gdy się obudzi losuje co dalej będzie robić; możliwości są 3:
		   i)   wraca do punktu a)
		   ii)  ginie natychmiast,
		   iii) idzie do punktu d) (planowane samobójstwo)
		d) losuje czas oczekiwania (T2)
		e) wysyła komunikat pożegnalny, wraz z T2 
		f) zasypia (na T2)
		g) po pobudce zamyka swoją stronę łącza i ginie.


	Uwaga II. Należy tak dobrać losowania, by i) było bardziej prawdopodobne od
	ii), a to bardziej niż iii).
***/
#include <iostream>
#include <sys/types.h>
#include <unistd.h>
#include <cstdlib>
#include <ctime>

using namespace std;

int main(int argc, char *argv[])
{
	int min_rand = 1;
	int max_rand = 10;
	int pipe_fd;
	pipe_fd = atoi(argv[1]);
	
	srand(time(NULL));
	// swoj pid
	// cout << "Moj PID: "<<getpid()<<endl;
	pid_t pid = getpid();

	write(pipe_fd,&pid,sizeof(pid));
	// losowanie ile czasu ma spac
	// a:
	// int T1 = rand() % max_rand + min_rand;
	// cout<< "Wylosowane spanko: "<<T1<<endl;
	// sleep(T1);
	// //ponowne losowanie
	// int rand_number = rand() % max_rand + min_rand;
	// cout<<getpid()<<" pospalem"<<endl<<endl; 
	
	// if(rand_number < 6) //i
	// {
	// 	cout<< "Opcja numer 1" <<endl;
	// 	goto a;
	// }
	// if(rand_number > 5 && rand_number < 9 ) // DRUGA OPCJA
	// {
	// 	cout<< "Opcja numer 2" <<endl;
	// 	cout<<"Czas umierac Panie "<<getpid()<<endl;
	// 	return 0;
	// }
	// if(rand_number > 8 && rand_number < 11) //TRZECIA OPCJA
	// {
	// 		cout<< "Opcja numer 3" <<endl;
	// 		int T2 = rand() % max_rand + min_rand;
	// 		//komunikat pozegnalny do dziadka razem z t2
	// 		sleep(T2);
	// 		cout<<"Czas umierac Panie "<<getpid()<<endl;
	// 		return 0;

	// }
	// return 0;
}