/****
	Program przyjmuje 2 parametry: -p lub -f oraz liczbę całkowitą.
	"-p" oznacza użycie potoków anonimowych (pipe), a "-f" kolejek fifo.
	Liczba oznacza ilość wnuków

	Program tworzy jednego potomka, a ten stosowaną ilość dalszych potomków
	("wnuków"). Bezpośredni potomek ginie od razu, po wygenerowaniu swoich
	potomków.
	Uwaga. Procesy "wnucząt" mają być stworzone jako osobne programy i uruchamiane
	poprzez exec.

	Proces główny:
		I. Przygotowania
		  - sprawdza poprawność parametrów (gdy są złe, to kończy działanie),
		  - przygotowuje odpowiednią ilość łączy komunikacyjnych
		    * jeżeli to mają być FIFO, to należy je przygotować w następujący sposób:
		      - utworzyć plik,
		      - otworzyć obie jego strony,
		      - usunąć plik
		II. Utworzenie potomka
		III. Podmiana na nowy program (exec).


****/
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

using namespace std;

int main(int argc, char* argv[])
{
	int flagP, flagF = 0;
	int pipe_fd[2];
	int number;
    int option;
    char bufor[100];
    char optstring[] = ":f:p:";

    while ( (option = getopt(argc,argv,optstring)) != -1)
    {
        switch ( option )
        {
	        case 'f' :
	            flagF = 1;
	           	number = (int)strtol(optarg, (char **)NULL, 10);
	            break;
	        case 'p' :
	            flagP = 1;  
	            number = (int)strtol(optarg, (char **)NULL, 10);
	            break;
	        case ':' : 
	            fprintf(stderr,"opcja -%c wymaga argumentu.\n KONIEC",optopt);
	            return 0;      
	        case '?' :
	        default :
	            fprintf(stderr, "opcja -%c nie znana – KONIEC\n", optopt);
	            return 0;
        }

    }

    pid_t childpid, grandchildpid;
  
    pipe(pipe_fd);
    switch(childpid = fork())
    {
	    default:
	    {
	        break;
	    }
	    case -1:
	    {
	        perror("Blad");
	        exit(1);
	    }
	    case 0:
	    {
	    	for(int i = 0; i < number; i++)
	    	{
	    		grandchildpid = fork();
				switch(grandchildpid)
			    {
				    default:
				    {
	        			    close(pipe_fd[0]);
						sprintf(bufor,"%i",pipe_fd[0]);
			            char *args[] = { "./new.o",bufor,0 };
			            execve(args[0], &args[0], NULL);
			            // exit(0);
				    }
				    case -1:
				    {
				        perror("Blad");
				        exit(1);
				    }
				    case 0:
				    {
				    	/*
				    	*
				    	POTOKI ANONIMOWE
				    	*
				    	*/
		                sprintf(bufor,"%i",pipe_fd[0]);
		                char *argv[] = { "./wnuki.o",bufor,0 };
		                execve(argv[0], &argv[0], NULL);
		                // exit(0);
				    }   
			    }
			}

	    }

    }

    return 0;
}